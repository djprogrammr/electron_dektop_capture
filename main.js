const electron = require('electron')
const {app, BrowserWindow} = electron

let mainWindow;
let modalWindow;

app.on('window-all-closed', () => {
  if (process.platform != 'darwin')
    app.quit();
});
console.log(electron);

app.setPath("userData", __dirname + "/saved_recordings");

app.on('ready', () => {
	var screen = electron.screen.getPrimaryDisplay().workAreaSize;
  mainWindow = new BrowserWindow({closable:false, modal:true,alwaysOnTop: true,width: screen.width, height: screen.height, show: true,frame: false,opacity:0.5 });
 
  mainWindow.loadURL('file://' + __dirname + '/index.html');
  setTimeout(function(){mainWindow.setFullScreen(true);},40000);
  mainWindow.on('closed', () => {
    mainWindow = null;
  });
});
