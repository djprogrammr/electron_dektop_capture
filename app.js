//copyright skillstack.com
var electron = require('electron');
const {dialog} = require('electron').remote;
var fs = require('fs');
var messenger = require('messenger');

let desktopSharing = true;
let localStream;
var access_token = "";
var public_ip="";
var testId = "";
var candidate_email="";
var type = "";  //mode of the launched machine(candidate/solution)
var inviteId = "";
var candidateName = "";
 

server = messenger.createListener(8000);
//Electron message passing 
server.on('Finish Test', function(message, data){
	console.log("got Finish test event");
	stopRecording();
  message.reply({'you':'youtube upload processing started'})
});
 

function onAccessApproved(desktop_id) {
  if (!desktop_id) {
    console.log('Desktop Capture access rejected.');
    return;
  }
  desktopSharing = true;
  document.querySelector('#start').innerHTML = "Stop Recording";
  console.log("Desktop sharing started.. desktop_id:" + desktop_id);
  navigator.webkitGetUserMedia({
    audio: false,
    video: {
      mandatory: {
        chromeMediaSource: 'desktop',
        chromeMediaSourceId: desktop_id,
        minWidth: 1280,
        maxWidth: 1280,
        minHeight: 720,
        maxHeight: 720
      }
    }
  }, startRecording, getUserMediaError);

  function startRecording(stream) {
    recorder = new MediaRecorder(stream);
    blobs = [];
    recorder.ondataavailable = function(event) {
        //console.log("on data:"+event.data);
		youtubeUpload(event.data);
        blobs.push(event.data);
    };
    recorder.start(5999 * 1000);
  }

  function getUserMediaError(e) {
    console.log('getUserMediaError: ' + JSON.stringify(e, null, '---'));
  }
}


 function toArrayBuffer(blob, cb) {
let fileReader = new FileReader();
fileReader.onload = function() {
    let arrayBuffer = this.result;
    cb(arrayBuffer);
};
fileReader.readAsArrayBuffer(blob);
}

  function toBuffer(ab) {
let buffer = new Buffer(ab.byteLength);
let arr = new Uint8Array(ab);
for (let i = 0; i < arr.byteLength; i++) {
    buffer[i] = arr[i];
}
return buffer;
}

$("#ok").click(function(){
	electron.remote.BrowserWindow.getFocusedWindow().minimize();
	$('#initialcontent').hide();
	$('#start').show();
    onAccessApproved('screen:0:0');
});

$(document).ready(function() {
	$("#start").hide();
	generateGoogleToken();
	//bypass user popup of asking screen share
	console.log(dialog);
	
	

	
	
    $.getJSON("https://api.ipify.org/?format=json", function (data) {
        console.log(data);
		public_ip = data.ip;
		getMachineDetails(public_ip);
    });


});

document.querySelector('#start').addEventListener('click', function(e) {
  stopRecording();
});

 function stopRecording(){
	desktopSharing = false;
    if (localStream)
		localStream.getTracks()[0].stop();
    localStream = null;
    document.querySelector('#start').innerHTML = "Done Recording";
	recorder.stop();
    toArrayBuffer(new Blob(blobs, {type: 'video/webm'}), function(ab) {
        var buffer = toBuffer(ab);
        var file = "C:\\electron\\old\\desktop-capture\\videos\\example.webm";
        fs.writeFile(file, buffer, function(err) {
            if (err) {
                console.error('Failed to save video ' + err);
            } else {
                console.log('Saved video: ' + file);
            }
        });
    });
	
 }
 
 window.addEventListener('beforeunload', function (event) {
     var answer = confirm('You cannot close this window');
	  event.returnValue = false;  
  });
  
//Candidate details
function getMachineDetails(ip){
	$.getJSON("http://www.skillstack.com:3000/api/candidates/gethostmachineuser?id="+ip, function (data) {
        console.log(data);
		testId = data.testId;
		type = data.type;
		inviteId = data.inviteId;
		candidateName = data.fullname;
		candidateId = data.candidateId;
		instanceId = data.instanceId;
    });
	
}

function updateReport(url){
	$.getJSON("http://www.skillstack.com:3000/api/candidates/updatescreenrecordingurl?inviteId="+inviteId+"&url="+url, function (data) {
        console.log(data);		
		createAMI();
    });
}

function createAMI(){
	 var solution = {id: '',
            testId: 0,
            instanceId: '',
            pwd: 'wEAgQDvzR7R%j2t%UnAgd=w)R-K&p$b6',
            envId: 0};
	solution.id = candidateId;
	solution.envId = 0 ;
	solution.instanceId = instanceId;
	solution.pwd = 'wEAgQDvzR7R%j2t%UnAgd=w)R-K&p$b6';
	
		var SERVER_URL='http://www.skillstack.com:3000/api/candidates/createami';
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var _testObj = JSON.parse(this.responseText);
     // _openQuestionPopup(_testObj.description);
	 // triggerFinishTest();
	  //showNotification('skillstack', 'Your solution has successfully been submitted.');
    }
  };
  xhttp.open("POST", SERVER_URL, true);
  xhttp.setRequestHeader('Content-Type', 'application/json');
  xhttp.send(JSON.stringify(solution));
}